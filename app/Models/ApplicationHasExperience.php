<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationHasExperience extends Model
{
    protected $table = 'application_has_experience';
    
    protected $guarded = [];

    public function application(){
    	return $this->belongsTo('App\Models\ApplicationForm');
	}
}

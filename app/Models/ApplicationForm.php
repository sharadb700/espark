<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationForm extends Model
{
    protected $table = 'application_form';
    
    protected $guarded = [];

    public static function find($data=[]){
        $response = Self::select('application_form.*');
        $response = $response->get();
        return $response;            
    }

    public static function findOne($id){
        $response = Self::select('application_form.*');
        $response = $response->where('id',$id);
        $response = $response->first();
        return $response;            
    }

    public function experience(){
    	return $this->belongsTo('App\Models\ApplicationHasExperience');
	}
}

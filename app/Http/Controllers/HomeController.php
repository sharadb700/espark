<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\Models\ApplicationForm;
use App\Models\ApplicationHasExperience;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function applicationList()
    {
        return view('applicationList');
    }

    public function getApplicationList(Request $request){

        $find = ApplicationForm::find();
      
        return Datatables::of($find)
            ->addColumn('created_at', function ($find) {
                return date('d M Y h:i A', strtotime($find->created_at));
            })
            ->addColumn('action', function ($find) {
                $edit = "<a href=''>Edit</a>";
                $delete = "<a href='".route('delete-application',$find->id)."' onclick='return confirm(\"Confirm deletion?\")'>Delete</a>";
                $action = $edit." ".$delete;
                return $action;
            })
            ->rawColumns(['action'])
            ->setTotalRecords($find->count())
            ->make(true);
    }

    public function deleteApplication($id){
        $row = ApplicationForm::findOne($id);
        if ($row == null) {
            session()->flash('error', 'Data not found.');
            return redirect()->route('application-list');
        }
        ApplicationForm::where('id', $id)->delete();
        ApplicationHasExperience::where('application_id', $id)->delete();
        session()->flash('success', "Successfully deleted.");
        return redirect()->route('application-list');
    }
}

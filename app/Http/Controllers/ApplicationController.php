<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use DataTables;
use App\Models\User;
use App\Models\ApplicationForm;
use App\Models\ApplicationHasExperience;

class ApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function post(Request $request){
        $rules = [
            'name' => 'required',
            'email'  => 'required', 
            'contact'  => 'required' 
        ];
        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {    
            
            $data = [
                'name'  => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'address' => $request->address,
                'gender' => $request->gender,
                'degree'     => isset($request->degree)?($request->degree):"",
                'year'     => isset($request->year)?($request->year):"",
                'per'     => isset($request->per)?($request->per):"",
                'hindi'     => isset($request->hindi)?implode(",",$request->hindi):"",
                'english'     => isset($request->english)?implode(",",$request->english):"",
                'gujrati'     => isset($request->gujrati)?implode(",",$request->gujrati):"",
                'technical'     => isset($request->technical)?implode(",",$request->technical):"",
                'php'     => isset($request->php)?($request->php):NULL,
                'mySql'     => isset($request->mySql)?($request->mySql):NULL,
                'laravel'     => isset($request->laravel)?($request->laravel):NULL,
                'oracle'     => isset($request->oracle)?($request->oracle):NULL,
                'location'     => isset($request->location)?($request->location):"",
                'expected'     => isset($request->expected)?($request->expected):"",
                'current'     => isset($request->current)?($request->current):"",
                'notice'     => isset($request->notice)?($request->notice):"",
            ];

            $application = ApplicationForm::create($data);

            if(isset($request->company) && !empty($request->company)){
                for($i=0;$i<count($request->company);$i++){
                    $experience=[
                        'application_id' => $application->id,
                        'company' => $request->company[$i],
                        'designation' => $request->designation[$i],
                        'from_date' => !empty($request->from[$i])?date('Y-m-d',strtotime($request->from[$i])):NULL,
                        'to_date' => !empty($request->to[$i])?date('Y-m-d',strtotime($request->to[$i])):NULL
                    ];
                   
                    ApplicationHasExperience::create($experience);
                }    
            }

            $request->session()->flash('success',"Application submitted successfully.");
            return redirect()->route('get-application');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }   
    }
}

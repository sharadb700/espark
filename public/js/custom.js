$(document).ready(function() {
    $("#application").validate({
        ignore: [],
        debug: false,
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            contact: {
                required: true,
            },
            address: {
                required: true,
            },
            degree: {
                required: true,
            },
            year: {
                required: true,
            },
            per: {
                required: true,
            },
            location: {
                required: true,
            },
            expected: {
                required: true,
            },
            current: {
                required: true,
            },
            notice: {
                required: true,
            },
        },
        highlight: function(e) {
            $(e).closest(".form-label-group").addClass("has-error")
        },
        success: function(e) {
            e.closest(".form-label-group").removeClass("has-error"), e.remove()
        },
        submitHandler: function(e) {
            e.submit()
        }
    });
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]*$/.test(value);
    });
    $("input.gujrati").attr("disabled", true);
    $("input.english").attr("disabled", true);
    $("input.hindi").attr("disabled", true);
    $("input.php").attr("disabled", true);
    $("input.mySql").attr("disabled", true);
    $("input.laravel").attr("disabled", true);
    $("input.oracle").attr("disabled", true);

    $('.gujrati-parent').on('change', function() {
        if (this.checked) {
            $("input.gujrati").removeAttr("disabled");
        } else {
            $("input.gujrati").attr("disabled", true);
            $('input.gujrati').prop('checked', false);
        }
    });
    $('.english-parent').on('change', function() {
        if (this.checked) {
            $("input.english").removeAttr("disabled");
        } else {
            $("input.english").attr("disabled", true);
            $('input.english').prop('checked', false);
        }
    });
    $('.hindi-parent').on('change', function() {
        if (this.checked) {
            $("input.hindi").removeAttr("disabled");
        } else {
            $("input.hindi").attr("disabled", true);
            $('input.hindi').prop('checked', false);
        }
    });
    $('.php-parent').on('change', function() {
        if (this.checked) {
            $("input.php").removeAttr("disabled");
        } else {
            $("input.php").attr("disabled", true);
            $('input.php').prop('checked', false);
        }
    });
    $('.mySql-parent').on('change', function() {
        if (this.checked) {
            $("input.mySql").removeAttr("disabled");
        } else {
            $("input.mySql").attr("disabled", true);
            $('input.mySql').prop('checked', false);
        }
    });
    $('.laravel-parent').on('change', function() {
        if (this.checked) {
            $("input.laravel").removeAttr("disabled");
        } else {
            $("input.laravel").attr("disabled", true);
            $('input.laravel').prop('checked', false);
        }
    });
    $('.oracle-parent').on('change', function() {
        if (this.checked) {
            $("input.oracle").removeAttr("disabled");
        } else {
            $("input.oracle").attr("disabled", true);
            $('input.oracle').prop('checked', false);
        }
    });
    $(".add-more").click(function(){ 
       var html = $(".copy").html();
       $(".after-add-more").after(html);
    });
    $("body").on("click",".remove",function(){ 
       $(this).parents(".control-group").remove();
    });
});
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
  'register' => false,
  'reset' => false,
  'verify' => false,
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/application-list', [App\Http\Controllers\HomeController::class, 'applicationList'])->name('application-list');
Route::post('/get-application-list', [App\Http\Controllers\HomeController::class, 'getApplicationList'])->name('get-application-list');
Route::get('/delete-application/{id}', [App\Http\Controllers\HomeController::class, 'deleteApplication'])->name('delete-application');
Route::get('/', [App\Http\Controllers\ApplicationController::class, 'index'])->name('get-application');
Route::post('/post-application', [App\Http\Controllers\ApplicationController::class, 'post'])->name('post-application');


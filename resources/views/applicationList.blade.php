@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<div class="container">
   @if (session('error'))
      <div class="alert alert-danger" role="alert">
          {{ session('error') }}
      </div>
  @endif
  @if (session('success'))
      <div class="alert alert-success" role="alert">
          {{ session('success') }}
      </div>
  @endif
  
   <table class="table table-bordered data-table" id="datatable">
      <thead>
         <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Conatct</th>
            <th>Address</th>
            <th>Register Time</th>
            <th width="150px">Action</th>
         </tr>
      </thead>
      <tbody>
      </tbody>
   </table>
</div>
<script>
   $(function() {
     var table = $('#datatable').DataTable({
       processing: true,
       serverSide: true,
         order: [[ 0, "desc" ]],
       ajax: {
           "url" : "{{ route('get-application-list') }}",
           "type": "POST",
           "data" : {
            "_token": "{{ csrf_token() }}"
           }
       },
       "lengthMenu": [
             [10, 15, 20, 50, 100, -1],
             [10, 15, 20, 50, 100, "All"]
         ],
       columns:[
                 { data: 'name', name: 'name' },
                 { data: 'email', name: 'email' },
                  { data: 'contact', name: 'contact' },
                 { data: 'address', name: 'address' },
                 { data: 'created_at', name: 'created_at' },
                 { data: 'action', name: 'action' },
               ]
     });
   });
</script>
@endsection
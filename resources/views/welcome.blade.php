<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Laravel</title>
      <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      <link href="{{asset('css/custom.css')}}" rel="stylesheet">
   </head>
   <body >
      <div class="container register">
         <div class="row">
            <div class="col-md-3 register-left">
               <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
               <h3>Welcome</h3>
               <a href="{{url('login')}}" class="login-btn">@auth Home @else Login @endauth</a>
               <br/>
            </div>
            <div class="col-md-9 register-right">

               <form action="{{route('post-application')}}" method="post" id="application">
                  @csrf
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                     <h3 class="register-heading">Apply</h3>
                     <div class="register-form">

                        <div class="col-md-12">
                           @if (session('error'))
                              <div class="alert alert-danger" role="alert">
                                  {{ session('error') }}
                              </div>
                          @endif
                          @if (session('success'))
                              <div class="alert alert-success" role="alert">
                                  {{ session('success') }}
                              </div>
                          @endif
                           <div class="form-group">
                              <input type="text" class="form-control" name="name" placeholder="Name *" value="" />
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="email" placeholder="Email *" value="" />
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="contact" minlength="10" maxlength="10" placeholder="Contact No *" value="" />
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" name="address"  placeholder="Address *" value="" />
                           </div>
                           <div class="form-group">
                              <div class="maxl">
                                 <label class="radio inline"> 
                                 <input type="radio" name="gender" value="male" checked>
                                 <span> Male </span> 
                                 </label>
                                 <label class="radio inline"> 
                                 <input type="radio" name="gender" value="female">
                                 <span>Female </span> 
                                 </label>
                              </div>
                           </div>
                        </div>
                        <hr>
                        <h4>Education details</h4>
                        <div class="form-group">
                           <select class="form-control" name="degree">
                              <option value="">Select Degree</option>
                              <option value="SSC">SSC</option>
                              <option value="HSC">HSC</option>
                              <option value="Graduation">Graduation</option>
                              <option value="Master">Master</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <select class="form-control" name="year">
                              <option value="">Select Year</option>
                              @for($i=1960;$i<=date('Y');$i++)
                              <option value="{{$i}}">{{$i}}</option>
                              @endfor
                           </select>
                        </div>
                        <div class="form-group">
                           <input type="text"  name="per" class="form-control" placeholder="CGPA/Percentage*" value="" />
                        </div>
                        <hr>
                        <h4>Work Experience</h4>
                        <div class="after-add-more">
                           <div class="form-group">
                              <input type="text"  name="company[]" class="form-control" placeholder="Company Name" value="" />
                           </div>
                           <div class="form-group">
                              <input type="text"  name="designation[]" class="form-control" placeholder="Designation" value="" />
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="date"  name="from[]" class="form-control" placeholder="From" value="" />
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <input type="date"  name="to[]" class="form-control" placeholder="To" value="" />
                                 </div>
                              </div>
                           </div>
                           <div class="input-group-btn"> 
                              <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                           </div>
                        </div>
                        <hr>
                        <h4>Known Languages</h4>
                        <div class="form-group">
                           <label class="checkbox-inline">
                           <input type="checkbox" name="hindi[]" value="1" class="hindi-parent">Hindi
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="hindi[]" value="2" class="hindi">Read
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="hindi[]" value="3" class="hindi">Write
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="hindi[]" value="4" class="hindi">Expert
                           </label>
                           <br>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="english[]" value="1" class="english-parent">English
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="english[]" value="2" class="english">Read
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="english[]" value="3" class="english">Write
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="english[]" value="4" class="english">Expert
                           </label>
                           <br>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="gujrati[]" value="1" class="gujrati-parent">Gujrati
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="gujrati[]" value="2" class="gujrati">Read
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="gujrati[]" value="3" class="gujrati">Write
                           </label>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="gujrati[]" value="4" class="gujrati">Expert
                           </label>
                        </div>
                        <hr>
                        <h4>Technical Experience</h4>
                        <div class="form-group">
                           <label class="checkbox-inline">
                           <input type="checkbox" name="technical[]" value="1" class="php-parent">PHP
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="php" value="1" class="php">Beginer
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="php" value="2" class="php">Mideator
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="php" value="3" class="php">Expert
                           </label>
                           <br>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="technical[]" value="2" class="mySql-parent">MySql
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="mySql" value="1" class="mySql">Beginer
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="mySql" value="2" class="mySql">Mideator
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="mySql" value="3" class="mySql">Expert
                           </label>
                           <br>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="technical[]" value="3" class="laravel-parent">Laravel
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="laravel" value="1" class="laravel">Beginer
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="laravel" value="2" class="laravel">Mideator
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="laravel" value="3" class="laravel">Expert
                           </label>
                           <br>
                           <label class="checkbox-inline">
                           <input type="checkbox" name="technical[]" value="4" class="oracle-parent">Oracle
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="oracle" value="1" class="oracle">Beginer
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="oracle" value="2" class="oracle">Mideator
                           </label>
                           <label class="checkbox-inline">
                           <input type="radio" name="oracle" value="3" class="oracle">Expert
                           </label>
                        </div>
                        <hr>
                        <h4>Preference</h4>
                        <div class="form-group">
                           <select class="form-control" name="location">
                              <option value="">Preferred Location</option>
                              <option value="Ahmedabad">Ahmedabad</option>
                              <option value="Surat">Surat</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="expected" placeholder=" Expected CTC *" value="" />
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="current" placeholder="Expected CTC *" value="" />
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="notice" placeholder="Notice Period *" value="" />
                        </div>
                        <div class="form-group">
                           <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </div>
                     </div>
                  </div>
               </form>
               <div class="copy hide">
                  <br>
                  <div class="control-group">
                     <div class="form-group">
                        <input type="text"  name="company[]" class="form-control" placeholder="Company Name" value="" />
                     </div>
                     <div class="form-group">
                        <input type="text"  name="designation[]" class="form-control" placeholder="Designation" value="" />
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <input type="date"  name="from[]" class="form-control" placeholder="From" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <input type="date"  name="to[]" class="form-control" placeholder="To" value="" />
                           </div>
                        </div>
                     </div>
                     <div class="input-group-btn"> 
                        <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
      <script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
   </body>
</html>
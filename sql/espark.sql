-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 26, 2021 at 11:55 AM
-- Server version: 8.0.21
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `espark`
--

-- --------------------------------------------------------

--
-- Table structure for table `application_form`
--

DROP TABLE IF EXISTS `application_form`;
CREATE TABLE IF NOT EXISTS `application_form` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `per` varchar(255) DEFAULT NULL,
  `hindi` varchar(255) DEFAULT NULL COMMENT '1= LanguageName,2= Read,3= Write,4=Expert',
  `english` varchar(255) DEFAULT NULL COMMENT '1= LanguageName,2= Read,3= Write,4=Expert',
  `gujrati` varchar(255) DEFAULT NULL COMMENT '1= LanguageName,2= Read,3= Write,4=Expert',
  `technical` varchar(255) DEFAULT NULL COMMENT '1 = PHP,2 = MySql,3= Laravel,4=Oracle',
  `php` int DEFAULT NULL COMMENT '1= Beginer,2= Mideator,3= Expert',
  `mySql` int DEFAULT NULL COMMENT '1= Beginer,2= Mideator,3= Expert',
  `laravel` int DEFAULT NULL COMMENT '1= Beginer,2= Mideator,3= Expert',
  `oracle` int DEFAULT NULL COMMENT '1= Beginer,2= Mideator,3= Expert',
  `location` varchar(255) DEFAULT NULL,
  `expected` varchar(255) DEFAULT NULL,
  `current` varchar(255) DEFAULT NULL,
  `notice` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_form`
--

INSERT INTO `application_form` (`id`, `name`, `email`, `contact`, `address`, `gender`, `degree`, `year`, `per`, `hindi`, `english`, `gujrati`, `technical`, `php`, `mySql`, `laravel`, `oracle`, `location`, `expected`, `current`, `notice`, `created_at`) VALUES
(1, 'Sharad Bharadwaj', 'sharad.cosmonautgroup@gmail.com', '1234567890', 'Tesla Society', 'male', 'SSC', '2015', '10', '1,2,3', '1,2', '1,2', '1,2,3,4', 1, 1, 1, 1, 'Ahmedabad', '10000', '12000', '2 Month', '2021-02-26 10:45:48'),
(3, 'Sharad Bharadwaj', 'demo@demo-sites.in', '1234567890', 'Ahmedabad', 'male', 'HSC', '1977', '10', '1,2,3', '1,2', '1', '1,2,3,4', 1, 2, 2, 2, 'Ahmedabad', '1000', '12000', '2 Month', '2021-02-26 11:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `application_has_experience`
--

DROP TABLE IF EXISTS `application_has_experience`;
CREATE TABLE IF NOT EXISTS `application_has_experience` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company` varchar(255) DEFAULT NULL,
  `designation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL,
  `application_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_has_experience`
--

INSERT INTO `application_has_experience` (`id`, `company`, `designation`, `from_date`, `to_date`, `created_at`, `updated_at`, `application_id`) VALUES
(1, 'Cosmonaut', 'PHP Developer', '2021-02-04', '2021-03-06', '2021-02-26 10:45:48', '2021-02-26 05:15:48', 1),
(2, 'Cosmonaut', 'PHP Developer', '2021-02-04', '2021-03-06', '2021-02-26 10:46:25', '2021-02-26 05:16:25', 2),
(3, 'Cosmonaut', 'PHP Developer', '2021-02-01', '2021-02-24', '2021-02-26 11:33:30', '2021-02-26 06:03:30', 3),
(4, 'Cosmonaut', 'PHP Developer', '2021-02-03', '2021-02-17', '2021-02-26 11:33:30', '2021-02-26 06:03:30', 3);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$zmWWgqVAJk5ftfRaPrvVtucQYnsLXHSVBK9mS3V2NPKFEHaQZGagC', NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
